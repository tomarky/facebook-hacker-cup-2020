import java.io.*;
import java.util.*;

class Solution
{
    static boolean greedy(int N, byte[] shards)
    {
        for (int i = 0; i < N - 2; i++)
        {
            if (shards[i] == shards[i+1] && shards[i] == shards[i+2])
            {
                continue;
            }
            if (shards[i + 1] == shards[i + 2])
            {
                shards[i] = shards[i + 1];
            }
            for (int j = i + 1; j < N - 2; j++)
            {
                shards[j] = shards[j + 2];
            }
            i = Math.max(-1, i - 3);
            N -= 2;
        }
        return N == 1;
    }

    static void test()
    {
        int N = 11;
        byte[] shards = new byte[N];
        boolean allOk = true;
        for (int i = 0; i < (1 << N); i++)
        {
            int countA = 0;
            for (int b = 0; b < N; b++)
            {
                int bit = (i >> b) & 1;
                shards[b] = (byte)bit;
                countA += bit;
            }
            int countB = N - countA;
            byte[] orig = Arrays.copyOf(shards, N);
            boolean ans = greedy(N, shards);
            boolean sug = Math.abs(countA - countB) == 1;
            if (ans != sug)
            {
                err.printf("NOT MATCH: %x%n", i);
                err.println("orig:   " + Arrays.toString(orig));
                err.println("shards: " + Arrays.toString(shards));
                err.println("countA: " + countA);
                err.println("countB: " + countB);
                err.println("ans: " + ans);
                err.println("sug: " + sug);
                allOk = false;
            }
        }
        if (allOk)
        {
            err.println("ALL OK!");
        }
    }

    static void solve() throws Exception
    {
        // solution
        int N = readInt();
        byte[] shards = in.readLine().getBytes();

        int countA = 0;
        for (int i = 0; i < N; i++)
        {
            if (shards[i] == 'A')
            {
                countA++;
            }
        }
        int countB = N - countA;

        if (Math.abs(countA - countB) == 1)
        {
            out.println("Y");
        }
        else
        {
            out.println("N");
        }
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        // test();

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}