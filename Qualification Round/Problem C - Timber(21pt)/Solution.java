import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int N = readInt();
        final int P = 0;
        final int H = 1;
        int[][] ph = new int[N][];
        for (int i = 0; i < N; i++)
        {
            ph[i] = readInts();
        }
        Arrays.sort(ph, (x, y) -> Integer.compare(x[P], y[P]));

        int ans = 0;

        // cut down to the right
        int[] ctiR = new int[N]; // Combined Timber Interval of cut down to the right
        Map<Integer, Integer> endRs = new TreeMap<>(); // right end
        for (int i = 0; i < N; i++)
        {
            ctiR[i] += ph[i][H];
            ans = Math.max(ans, ctiR[i]);
            int endR = ph[i][P] + ph[i][H];
            Integer value = endRs.get(endR);
            if (value == null || ctiR[i] > value)
            {
                endRs.put(endR, ctiR[i]);
            }
            int j = Arrays.binarySearch(ph, new int[]{endR, endR}, (x, y) -> Integer.compare(x[P], y[P]));
            if (j >= 0 && j < N)
            {
                ctiR[j] = Math.max(ctiR[j], ctiR[i]);
            }
        }

        // cut down to the left
        int[] ctiL = new int[N]; // Combined Timber Interval of cut down to the left
        for (int i = N-1; i >= 0; i--)
        {
            ctiL[i] += ph[i][H];
            ans = Math.max(ans, ctiL[i]);
            int endL = ph[i][P] - ph[i][H];
            Integer value = endRs.get(endL);
            if (value != null)
            {
                ans = Math.max(ans, ctiL[i] + value);
            }
            int j = Arrays.binarySearch(ph, new int[]{endL, endL}, (x, y) -> Integer.compare(x[P], y[P]));
            if (j >= 0 && j < N)
            {
                ctiL[j] = Math.max(ctiL[j], ctiL[i]);
            }
        }

        out.println(ans);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}