import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int N = readInt();
        byte[] to = in.readLine().getBytes();
        byte[] from = in.readLine().getBytes();

        out.println();
        for (int i = 0; i < N; i++)
        {
            boolean movable = true;
            StringBuilder sb = new StringBuilder();
            for (int j = i; j > 0; j--)
            {
                if (movable && from[j] == 'Y' && to[j - 1] == 'Y')
                {
                    sb.append('Y');
                }
                else
                {
                    sb.append('N');
                    movable = false;
                }
            }
            sb.reverse();
            sb.append('Y');
            movable = true;
            for (int j = i + 1; j < N; j++)
            {
                if (movable && from[j - 1] == 'Y' && to[j] == 'Y')
                {
                    sb.append('Y');
                }
                else
                {
                    sb.append('N');
                    movable = false;
                }
            }
            out.println(sb.toString());
        }
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}