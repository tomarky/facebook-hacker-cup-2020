import java.io.*;
import java.util.*;

class Solution
{
    static long[] minTree = new long[1024*1024*2];
    static long[] maxTree = new long[1024*1024*2];

    static void initTree()
    {
        Arrays.fill(minTree, Long.MAX_VALUE);
        Arrays.fill(maxTree, Long.MAX_VALUE);
    }

    static long get(int i)
    {
        int x = 0;
        int w = 1024 * 512;
        int p = 0;
        for (;;)
        {
            if (minTree[p] == maxTree[p])
            {
                return minTree[p];
            }
            if (i < x + w)
            {
                p = 2 * p + 1;
            }
            else
            {
                x += w;
                p = 2 * p + 2;
            }
            w >>= 1;
        }
    }

    static void set(int x, int w, int p, int s, int e, long value)
    {
        if (value >= maxTree[p])
        {
            return;
        }
        if (w == 0 || (x == s && x + w * 2 - 1 == e && value < minTree[p]))
        {
            minTree[p] = Math.min(minTree[p], value);
            maxTree[p] = minTree[p];
            return;
        }
        if (value < minTree[p])
        {
            minTree[p] = value;
        }
        if (e < x + w)
        {
            set(x, w >> 1, 2 * p + 1, s, e, value);
        }
        else if (x + w <= s)
        {
            set(x + w, w >> 1, 2 * p + 2, s, e, value);
        }
        else
        {
            set(x, w >> 1, 2 * p + 1, s, x + w - 1, value);
            set(x + w, w >> 1, 2 * p + 2, x + w, e, value);
        }
    }

    static void set(int s, int e, long value)
    {
        if (s < e)
        {
            set(0, 1024 * 512, 0, s, e, value);
        }
    }

    static void solve() throws Exception
    {
        // solution
        int[] nm = readInts();
        int n = nm[0], m = nm[1];
        long[] test = new long[n];
        for (int i = 0; i < n; i++)
        {
            test[i] = readLong();
        }

        initTree();

        // n = 1_000_000;
        // m = 100000;
        // test = new long[n];
        // Random rand = new Random();
        // for (int i = 0; i < n; i++)
        // {
            // test[i] = Math.abs(rand.nextLong() >> 1) % 1_000_000_000L + 1L;
        // }

        set(0, Math.min(n-1,m), 0L);
        boolean ok = true;
        for (int i = 0; i < n; i++)
        {
            long cost = test[i];
            if (!ok)
            {
                continue;
            }
            long cur = get(i);
            if (cur == Long.MAX_VALUE)
            {
                ok = false;
                continue;
            }
            if (cost == 0L)
            {
                continue;
            }
            set(i, Math.min(n-1,i+m), cur + cost);
        }

        if (ok)
        {
            out.println(get(n-1));
        }
        else
        {
            out.println("-1");
        }
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static long readLong() throws Exception
    {
        return Long.parseLong(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }
}