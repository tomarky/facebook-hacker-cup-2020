Facebook Hacker Cup 2020に参加したときのコード

Qualification Round	2020/07/25-2020/07/27 46pt 1432位 (8:51:24)
  Problem A: Travel Restrictions             10pt / 10pt
  Problem B: Alchemy                         15pt / 15pt
  Problem C: Timber                          21pt / 21pt
  Problem D1: Running on Fumes - Chapter 1    0pt / 16pt (Presentation Error)
  Problem D2: Running on Fumes - Chapter 2    -pt / 38pt (not challenge)
https://www.facebook.com/codingcompetitions/hacker-cup/2020/qualification-round
https://www.facebook.com/codingcompetitions/hacker-cup/2020/qualification-round/scoreboard

Round 1 2020/08/16 2:00:00 - 2020/08/17 2:00:00 0pt
  Problem A1: Perimetric - Chapter 1          0pt / 10pt (Presentation Error)
  Problem A2: Perimetric - Chapter 2          -pt / 15pt (not challenge)
  Problem A3: Perimetric - Chapter 3          -pt / 17pt (not challenge)
  Problem B:  Dislodging Logs                 -pt / 20pt (not challenge)
  Problem C:  Quarantine                      -pt / 38pt (not challenge)
https://www.facebook.com/codingcompetitions/hacker-cup/2020/round-1
https://www.facebook.com/codingcompetitions/hacker-cup/2020/round-1/scoreboard