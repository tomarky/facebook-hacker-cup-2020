import java.io.*;
import java.util.*;

class Solution
{
    static final long MOD = 1_000_000_007L;

    static void solve() throws Exception
    {
        // solution
        int[] nkw = readInts();
        int N = nkw[0], K = nkw[1];
        int K2 = K + 22;
        long W = (long)nkw[2];
        long[] Lk = readLongs(new long[K2]);
        long[] Labcd = readLongs();
        long AL = Labcd[0];
        long BL = Labcd[1];
        long CL = Labcd[2];
        long DL = Labcd[3];
        long[] Hk = readLongs(new long[K2]);
        long[] Habcd = readLongs();
        long AH = Habcd[0];
        long BH = Habcd[1];
        long CH = Habcd[2];
        long DH = Habcd[3];

        long pSep = 0L;
        long pGrow = (W + Hk[0]) * 2L % MOD;
        long x0 = Lk[0], x1 = Lk[0] + W, h = Hk[0];

        long answer = pGrow;

        for (int i = 1; i < K; i++)
        {
            if (x1 < Lk[i])
            {
                pSep = (pSep + pGrow) % MOD;
                pGrow = (W + Hk[i]) * 2L % MOD;
                x1 = Lk[i] + W;
            }
            else
            {
                h = 0L;
                for (int j = i - 1; j >= 0; j--)
                {
                    if (Lk[j] + W < Lk[i])
                    {
                        break;
                    }
                    h = Math.max(h, Hk[j]);
                }
                if (h < Hk[i])
                {
                    pGrow += (Hk[i] - h) * 2L % MOD;
                }
                pGrow += (Lk[i] + W - x1) * 2L % MOD;
                pGrow %= MOD;
            }
            x1 = Lk[i] + W;
            long p = (pSep + pGrow) % MOD;
            answer = (answer * p) % MOD;
        }

        for (int i = K, e = K; e < N; e++, i++) {
            if (i == K2)
            {
                i = 0;
            }
            int i1 = i - 1, i2 = i - 2;
            if (i1 < 0)
            {
                i1 += K2;
            }
            if (i2 < 0)
            {
                i2 += K2;
            }
            Lk[i] = (AL * Lk[i2] % DL + BL * Lk[i1] % DL + CL) % DL + 1L;
            Hk[i] = (AH * Hk[i2] % DH + BH * Hk[i1] % DH + CH) % DH + 1L;
            if (x1 < Lk[i])
            {
                pSep = (pSep + pGrow) % MOD;
                pGrow = (W + Hk[i]) * 2L % MOD;
                x1 = Lk[i] + W;
            }
            else
            {
                h = 0L;
                for (int j = i;;)
                {
                    j--;
                    if (j < 0)
                    {
                        j += K2;
                    }
                    if (Lk[j] + W < Lk[i])
                    {
                        break;
                    }
                    h = Math.max(h, Hk[j]);
                }
                if (h < Hk[i])
                {
                    pGrow += (Hk[i] - h) * 2L % MOD;
                }
                pGrow += (Lk[i] + W - x1) * 2L % MOD;
                pGrow %= MOD;
            }
            x1 = Lk[i] + W;
            long p = (pSep + pGrow) % MOD;
            answer = (answer * p) % MOD;
        }

        out.println(answer);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < tokens.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        return readLongs(null);
    }

    static long[] readLongs(long[] ret) throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        if (ret == null)
        {
            ret = new long[tokens.length];
        }
        for (int i = 0; i < tokens.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}